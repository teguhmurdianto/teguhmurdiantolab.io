FROM nginx:stable-alpine
WORKDIR /usr/share/nginx/html
COPY ./public/ /usr/share/nginx/html
COPY nginx/nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
